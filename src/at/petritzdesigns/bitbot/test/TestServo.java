package at.petritzdesigns.bitbot.test;

import at.petritzdesigns.bitbot.control.components.Servo;

/**
 * TestServo
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class TestServo implements Servo {

    @Override
    public void setDegree(int degrees) {
        System.out.println("degree "+ degrees);
    }

    @Override
    public void setPulseWidth(int width) {
        System.out.println("pulse width " + width);
    }

}
