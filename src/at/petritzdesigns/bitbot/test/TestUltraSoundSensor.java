package at.petritzdesigns.bitbot.test;

import at.petritzdesigns.bitbot.control.components.UltraSoundSensor;

/**
 * TestUltraSoundSensor
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class TestUltraSoundSensor implements UltraSoundSensor {

    @Override
    public long getDistance() {
        return 200;
    }

}
