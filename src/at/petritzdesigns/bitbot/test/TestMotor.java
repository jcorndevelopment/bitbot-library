package at.petritzdesigns.bitbot.test;

import at.petritzdesigns.bitbot.control.components.Motor;
import at.petritzdesigns.bitbot.exceptions.BitBotControlException;

/**
 * TestMotor
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class TestMotor implements Motor {

    private final String name;

    public TestMotor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean setup() throws BitBotControlException {
        System.out.println("setup "+ name);
        return true;
    }

    @Override
    public boolean move(int speed) throws BitBotControlException {
        System.out.println("move " + speed + " " + name);
        return true;
    }

    @Override
    public boolean shutdown() throws BitBotControlException {
        System.out.println("shutdown " + name);
        return true;
    }
}
