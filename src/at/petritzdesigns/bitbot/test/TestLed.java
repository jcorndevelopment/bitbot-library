package at.petritzdesigns.bitbot.test;

import at.petritzdesigns.bitbot.control.components.Led;

/**
 * TestLed
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class TestLed implements Led {

    @Override
    public void pulse(long duration) {
        System.out.println("pulse " + duration);
    }

    @Override
    public void toggle() {
        System.out.println("toggle");
    }

    @Override
    public void turnOff() {
        System.out.println("turn off");
    }

    @Override
    public void turnOn() {
        System.out.println("turn on");
    }

}
