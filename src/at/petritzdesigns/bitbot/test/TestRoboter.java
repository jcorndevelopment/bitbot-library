package at.petritzdesigns.bitbot.test;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.control.components.IRSensor;
import at.petritzdesigns.bitbot.control.components.Led;
import at.petritzdesigns.bitbot.control.components.Motor;
import at.petritzdesigns.bitbot.control.components.Servo;
import at.petritzdesigns.bitbot.control.components.Switch;
import at.petritzdesigns.bitbot.control.components.UltraSoundSensor;
import at.petritzdesigns.bitbot.exceptions.BitBotControlException;
import java.io.PrintStream;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class TestRoboter implements Roboter {

    /**
     * Printstream to write outputs
     */
    private final PrintStream out;

    /**
     * Default constructor<br>
     * uses default out
     */
    public TestRoboter() {
        this.out = System.out;
    }

    /**
     * Constructor that sets printstream
     *
     * @param out printstream
     */
    public TestRoboter(PrintStream out) {
        this.out = out;
    }

    /**
     * Just outputs method name (testing only)
     *
     * @return always true
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean setup() throws BitBotControlException {
        out.println("setup");
        return true;
    }

    /**
     * Just outputs method name (testing only)
     *
     * @return always true
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean shutdown() throws BitBotControlException {
        out.println("shutdown");
        return true;
    }

    @Override
    public Motor getLeftMotor() throws BitBotControlException {
        return new TestMotor("left");
    }

    @Override
    public Motor getRightMotor() throws BitBotControlException {
        return new TestMotor("right");
    }

    @Override
    public UltraSoundSensor getUltraSoundSensor() {
        return new TestUltraSoundSensor();
    }

    @Override
    public Switch getSwitch() {
        return new TestSwitch();
    }

    @Override
    public Led getFrontLeds() {
        return new TestLed();
    }

    @Override
    public Led getRearLeds() {
        return new TestLed();
    }

    @Override
    public IRSensor getLineLeftIRSensor() {
        return new TestIRSensor();
    }

    @Override
    public IRSensor getLineRightIRSensor() {
        return new TestIRSensor();
    }

    @Override
    public IRSensor getLeftIRSensor() {
        return new TestIRSensor();
    }

    @Override
    public IRSensor getRightIRSensor() {
        return new TestIRSensor();
    }

    @Override
    public Servo getPanServo() {
        return new TestServo();
    }

    @Override
    public Servo getTiltServo() {
        return new TestServo();
    }

}
