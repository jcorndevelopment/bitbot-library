package at.petritzdesigns.bitbot.test;

import at.petritzdesigns.bitbot.control.components.IRSensor;
import at.petritzdesigns.bitbot.control.listener.IRSensorListener;

/**
 * TestIRSensor
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class TestIRSensor implements IRSensor {

    @Override
    public boolean isTriggered() {
        return true;
    }

    @Override
    public void addListener(IRSensorListener listener) {
    }

    @Override
    public void removeListener(IRSensorListener listener) {
    }

}
