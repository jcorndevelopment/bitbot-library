package at.petritzdesigns.bitbot.test;

import at.petritzdesigns.bitbot.control.components.Switch;
import at.petritzdesigns.bitbot.control.listener.SwitchListener;

/**
 * TestSwitch
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class TestSwitch implements Switch {

    @Override
    public void addListener(SwitchListener listener) {
    }

    @Override
    public long getLastPressionMillisec() {
        return 0;
    }

    @Override
    public boolean isPushed() {
        return false;
    }

    @Override
    public void removeListener(SwitchListener listener) {
    }

    @Override
    public void shutdown() {
    }

}
