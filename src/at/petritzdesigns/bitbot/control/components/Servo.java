package at.petritzdesigns.bitbot.control.components;

/**
 * An abstract servo motor.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public interface Servo {

    /**
     * Set the degree of the servo motor
     *
     * @param degrees the degrees
     */
    void setDegree(int degrees);

    /**
     * Set the pulse width of the servo motor
     *
     * @param width the width
     */
    void setPulseWidth(int width);
}
