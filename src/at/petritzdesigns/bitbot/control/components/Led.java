package at.petritzdesigns.bitbot.control.components;

/**
 * An abstract LED.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public interface Led {

    /**
     * Pulse the led for a specific duration
     *
     * @param duration the duration
     */
    void pulse(long duration);

    /**
     * Toggle the led status
     */
    void toggle();

    /**
     * Turn the LED off
     */
    void turnOff();

    /**
     * Turn the LED on
     */
    void turnOn();
}
