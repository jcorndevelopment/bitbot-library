package at.petritzdesigns.bitbot.control.components;

import at.petritzdesigns.bitbot.control.listener.SwitchListener;
import java.util.EventListener;

/**
 * An abstract switch/button.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public interface Switch extends EventListener {

    /**
     * Add Switch Listener
     *
     * @param listener the listener
     */
    void addListener(final SwitchListener listener);

    /**
     * Get last press in milliseconds
     *
     * @return the last pression in ms
     */
    long getLastPressionMillisec();

    /**
     * Check if the switch is pushed
     *
     * @return if pushed or not
     */
    boolean isPushed();

    /**
     * Remove Switch Listener
     *
     * @param listener the listener
     */
    void removeListener(final SwitchListener listener);

    /**
     * Shutdown switch
     */
    void shutdown();
}
