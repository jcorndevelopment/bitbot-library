package at.petritzdesigns.bitbot.control.components;

import at.petritzdesigns.bitbot.exceptions.BitBotControlException;

/**
 * An abstract DC motor.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public interface Motor {

    /**
     * Setup everything to control motor afterwards
     *
     * @return success
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotControlException when
     * something failed
     */
    boolean setup() throws BitBotControlException;

    /**
     * Move motor
     *
     * @param speed Speed of the motor<br>
     * -100 - 0: Backwards<br>
     * 0 - 100: Forwards<br>
     * 0: Stop<br>
     * @return success
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotControlException when
     * something failed
     */
    boolean move(int speed) throws BitBotControlException;

    /**
     * Shutdowns motor
     *
     * @return success
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotControlException when
     * something failed
     */
    boolean shutdown() throws BitBotControlException;
}
