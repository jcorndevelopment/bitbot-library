package at.petritzdesigns.bitbot.control.components;

import at.petritzdesigns.bitbot.control.listener.IRSensorListener;
import java.util.EventListener;

/**
 * An abstract infrared sensor.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public interface IRSensor extends EventListener {

    /**
     * Is the IR Sensor triggered
     *
     * @return triggered
     */
    boolean isTriggered();

    /**
     * Add listener
     *
     * @param listener the listener
     */
    void addListener(final IRSensorListener listener);

    /**
     * Remove listener
     *
     * @param listener the listener
     */
    void removeListener(final IRSensorListener listener);
}
