package at.petritzdesigns.bitbot.control.components;

/**
 * An abstract ultra sonic sensor.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public interface UltraSoundSensor {

    /**
     * Get the distance
     *
     * @return the distance
     */
    long getDistance();
}
