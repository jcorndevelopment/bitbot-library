package at.petritzdesigns.bitbot.control.listener;

/**
 * The listener interface for the switch.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public interface SwitchListener {

    /**
     * Event when the switch state is changed
     *
     * @param isPressed if the switch is pressed
     */
    public void changedEvent(boolean isPressed);
}
