package at.petritzdesigns.bitbot.control.listener;

/**
 * The listener interface for the infrared sensor.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public interface IRSensorListener {

    /**
     * Is the IR sensor triggered
     *
     * @param isFired if the ir sensor is on
     */
    public void triggered(boolean isFired);
}
