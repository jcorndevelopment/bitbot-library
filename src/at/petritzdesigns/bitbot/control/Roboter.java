package at.petritzdesigns.bitbot.control;

import at.petritzdesigns.bitbot.control.components.IRSensor;
import at.petritzdesigns.bitbot.control.components.Led;
import at.petritzdesigns.bitbot.control.components.Motor;
import at.petritzdesigns.bitbot.control.components.Servo;
import at.petritzdesigns.bitbot.control.components.Switch;
import at.petritzdesigns.bitbot.control.components.UltraSoundSensor;
import at.petritzdesigns.bitbot.exceptions.BitBotControlException;

/**
 * An abstract roboter with abstract components.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public interface Roboter {

    /**
     * Setup everything to control roboter afterwards
     *
     * @return success
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotControlException when
     * something failed
     */
    boolean setup() throws BitBotControlException;

    /**
     * Shutdowns roboter
     *
     * @return success
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotControlException when
     * something failed
     */
    boolean shutdown() throws BitBotControlException;

    /**
     * Gets the left motor
     *
     * @return motor
     * @throws BitBotControlException on errors
     */
    Motor getLeftMotor() throws BitBotControlException;

    /**
     * Gets the right motor
     *
     * @return motor
     * @throws BitBotControlException on errors
     */
    Motor getRightMotor() throws BitBotControlException;

    /**
     * Gets the ultra sound sensor
     *
     * @return ultra sound sensor
     */
    UltraSoundSensor getUltraSoundSensor();

    /**
     * Gets the switch
     *
     * @return switch
     */
    Switch getSwitch();

    /**
     * Get the front LEDs
     *
     * @return front leds
     */
    Led getFrontLeds();

    /**
     * Get the rears LEDs
     *
     * @return rear leds
     */
    Led getRearLeds();

    /**
     * Get the line left IR sensor
     *
     * @return left line ir sensor
     */
    IRSensor getLineLeftIRSensor();

    /**
     * Get the line right IR sensor
     *
     * @return right line ir sensor
     */
    IRSensor getLineRightIRSensor();

    /**
     * Get the left IR sensor
     *
     * @return left ir sensor
     */
    IRSensor getLeftIRSensor();

    /**
     * Get the right IR sensor
     *
     * @return right ir sensor
     */
    IRSensor getRightIRSensor();

    /**
     * Get the pan servo
     *
     * @return pan servo
     */
    Servo getPanServo();

    /**
     * Get the tilt servo
     *
     * @return tilt servo
     */
    Servo getTiltServo();
}
