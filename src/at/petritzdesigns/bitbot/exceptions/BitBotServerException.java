package at.petritzdesigns.bitbot.exceptions;

/**
 * This exception should be thrown, whenever a server exception occured.
 *
 * @author Markus Petritz
 * @author Julian Maierl
 * @version 1.0.0
 */
public class BitBotServerException extends BitBotException {

    private final Exception initialException;

    /**
     * Empty constructor
     */
    public BitBotServerException() {
        this.initialException = null;
    }

    /**
     * Constructor with a message
     *
     * @param msg the detail message.
     */
    public BitBotServerException(String msg) {
        super(msg);
        this.initialException = null;
    }

    /**
     * Constructor with the initial exception
     *
     * @param initialException the initial exception
     */
    public BitBotServerException(Exception initialException) {
        this.initialException = initialException;
    }

    /**
     * Constructor with the initial exception and the message
     *
     * @param initialException initial exception
     * @param msg message
     */
    public BitBotServerException(Exception initialException, String msg) {
        super(msg);
        this.initialException = initialException;
    }

    /**
     * Has initial exception
     *
     * @return boolean
     */
    public boolean hasInitialException() {
        return initialException != null;
    }

    /**
     * Return initial exception
     *
     * @return exception
     */
    public Exception getInitialException() {
        return initialException;
    }
}
