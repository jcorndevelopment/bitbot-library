package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.exceptions.BitBotException;
import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.CommandDataFactory;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.ServerPort;
import at.petritzdesigns.bitbot.networking.data.commands.CloseConnectionCommand;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The BitBot server implementation for the TCP protocol.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotTcpServer extends BitBotServer {

    /**
     * Simulation constructor<br>
     * Listens to simulation port
     *
     * @param robot simulation roboter
     */
    public BitBotTcpServer(Roboter robot) {
        super(robot, ServerPort.SIMULATION_PORT.getPort());
    }

    /**
     * Custom Constructor
     *
     * @param robot custom robot
     * @param port custom port
     */
    public BitBotTcpServer(Roboter robot, int port) {
        super(robot, port);
    }

    /**
     * Get the server
     *
     * @return server
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    @Override
    protected AbstractServer getServer() throws BitBotServerException {
        return new TcpServer(port);
    }

    /**
     * Inner class to serve the clients!
     */
    private class TcpServer extends AbstractServer {

        private BufferedReader br;
        private PrintStream ps;
        private ServerSocket serverSocket;

        public TcpServer(int port) throws BitBotServerException {
            super(port);
            try {
                serverSocket = new ServerSocket(port);
                running = true;
            } catch (IOException ex) {
                throw new BitBotServerException(ex, "Could not start the server!");
            }
        }

        @Override
        public void run() {
            while (true) {
                Socket socket = null;
                try {
                    socket = serverSocket.accept();
                    br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    ps = new PrintStream(socket.getOutputStream());
                    while (running) {
                        String line = br.readLine();
                        if (line.isEmpty()) {
                            continue;
                        }
                        try {
                            CommandData command = BitBotCommandProtocol.parseCommand(line);
                            System.out.println("Command: [" + line + "]");
                            ResponseData response = executor.run(command);
                            ps.println(response.toString());
                            ps.flush();
                        } catch (BitBotException ex) {
                            //Logger.e("Could not parse command " + ex);
                        }
                    }
                } catch (IOException e) {
                    System.err.println("Cannot listen to clients!" + e);
                } catch (Exception ex) {
                    //Hacker tried to hack us xD
                    System.err.println(ex);
                    try {
                        executor.run(new CommandDataFactory().getCommandData(CloseConnectionCommand.class));
                        socket.close();
                        br.close();
                        ps.close();
                    } catch (Exception e) {
                    }
                }
            }
        }

    }
}
