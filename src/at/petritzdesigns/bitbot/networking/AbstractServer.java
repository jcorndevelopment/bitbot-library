package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.exceptions.BitBotServerException;

/**
 * The abstract server class. Mostly the inner class of a BitBotServer that is
 * executed in a new thread.
 *
 * @see BitBotServer
 * @author Markus Petritz
 * @version 1.0.0
 */
public abstract class AbstractServer implements Runnable {

    private final int port;

    public AbstractServer(int port) throws BitBotServerException {
        this.port = port;
    }

}
