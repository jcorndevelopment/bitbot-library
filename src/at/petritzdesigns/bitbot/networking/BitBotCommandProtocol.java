package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.exceptions.BitBotException;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.Responses;
import at.petritzdesigns.bitbot.networking.data.responses.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The communication protocol for all network traffic. It has methods for
 * parsing Commands and Responses.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotCommandProtocol {

    /**
     * Regex to identify commands
     */
    private static final Pattern COMMAND_PATTERN;

    /**
     * Static initializer
     */
    static {
        COMMAND_PATTERN = Pattern.compile("([0-9]*)\\|([0-9]*)\\|(([\\;a-zA-Z0-9-]*))\\|bb");
    }

    /**
     * Parses command
     *
     * @param line the input
     * @return parsed command data
     * @throws BitBotException when something failed
     */
    public static CommandData parseCommand(String line) throws BitBotException {
        if (line == null || line.isEmpty()) {
            throw new BitBotException("Invalid Command");
        }

        Matcher m = COMMAND_PATTERN.matcher(line);
        if (!m.find()) {
            throw new BitBotException("Wrong Command Pattern: [" + line + "]");
        }

        String idStr = m.group(1);
        String seqStr = m.group(2);
        String paramStr = m.group(3);

        byte id = Byte.parseByte(idStr);
        short seq = Short.parseShort(seqStr);

        String[] param;
        if (!paramStr.isEmpty()) {
            param = paramStr.split(";");
        } else {
            param = new String[0];
        }
        return new CommandData(id, seq, new ArrayList<>(Arrays.asList(param)));
    }

    public static Response parseResponse(String line) throws BitBotException, Exception {
        if (line == null || line.isEmpty()) {
            throw new BitBotException("Invalid Response");
        }

        Matcher m = COMMAND_PATTERN.matcher(line);
        if (!m.find()) {
            throw new BitBotException("Wrong Command Pattern: [" + line + "]");
        }

        String idStr = m.group(1);
        String seqStr = m.group(2);
        String paramStr = m.group(3);

        byte id = Byte.parseByte(idStr);
        short seq = Short.parseShort(seqStr);

        String[] param;
        if (!paramStr.isEmpty()) {
            param = paramStr.split(";");
        } else {
            param = new String[0];
        }

        return Responses.getInstance().getClassByIdentifier(id).parse(seq, new ArrayList<>(Arrays.asList(param)));
    }
}
