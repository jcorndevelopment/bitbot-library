package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.exceptions.BitBotException;
import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.CommandDataFactory;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.ServerPort;
import at.petritzdesigns.bitbot.networking.data.commands.CloseConnectionCommand;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * The BitBot server implementation for the UDP protocol.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotUdpServer extends BitBotServer {

    /**
     * Size of the message buffer
     */
    private static final int BUFFERSIZE = 512;

    /**
     * Simulation constructor<br>
     * Listens to simulation port
     *
     * @param robot simulation roboter
     */
    public BitBotUdpServer(Roboter robot) {
        super(robot, ServerPort.SIMULATION_PORT.getPort());
    }

    /**
     * Custom Constructor
     *
     * @param robot custom robot
     * @param port custom port
     */
    public BitBotUdpServer(Roboter robot, int port) {
        super(robot, port);
    }

    /**
     * Get the server
     *
     * @return server
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    @Override
    protected AbstractServer getServer() throws BitBotServerException {
        return new UdpServer(port);
    }

    /**
     * Inner class to serve the clients!
     */
    private class UdpServer extends AbstractServer {

        private DatagramSocket server;

        public UdpServer(int port) throws BitBotServerException {
            super(port);
            try {
                server = new DatagramSocket(port);
                running = true;
                //iwi an InputStream an den CommandExecutor übergeben wie @BitBotTccpServer
            } catch (SocketException e) {
                throw new BitBotServerException(e, "Could not start the server!");
            }
        }

        @Override
        public void run() {
            byte[] receiveData = new byte[BUFFERSIZE];
            byte[] sendData = new byte[BUFFERSIZE];

            while (running) {
                try {
                    DatagramPacket packet = new DatagramPacket(receiveData, receiveData.length);
                    server.receive(packet);
                    String data = new String(packet.getData());
                    if (data.isEmpty()) {
                        continue;
                    }
                    System.out.println("Command: [" + data + "]");
                    CommandData command = BitBotCommandProtocol.parseCommand(data);
                    
                    ResponseData response = executor.run(command);
                    sendData = response.toString().getBytes();
                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, server.getInetAddress(), port);
                    server.send(sendPacket);
                } catch (IOException ex) {
                    //Packet thrown away because of UDP xD
                    System.err.println(ex);
                } catch (BitBotException ex) {
                    //Hacker tried to hack us xD
                    //Logger.e(ex);
                } catch (Exception ex) {
                    //Hacker tried to hack us xD
                    System.err.println(ex);
                    try {
                        executor.run(new CommandDataFactory().getCommandData(CloseConnectionCommand.class));
                        server.close();
                        
                    } catch (Exception e) {
                    }
                }
            }
        }

    }
}
