package at.petritzdesigns.bitbot.networking.data;

/**
 * The server port configuration.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public enum ServerPort {

    /**
     * Default BitBot UDP Server Port
     */
    DEFAULT_UDP_PORT(10000),
    /**
     * Default BitBot TCP Server Port
     */
    DEFAULT_TCP_PORT(10001),
    /**
     * Default BitBot Simulation Ports
     */
    SIMULATION_PORT(7272);

    private final int port;

    private ServerPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }
}
