package at.petritzdesigns.bitbot.networking.data;

import at.petritzdesigns.bitbot.networking.data.commands.CloseConnectionCommand;
import at.petritzdesigns.bitbot.networking.data.commands.Command;
import at.petritzdesigns.bitbot.networking.data.commands.ControlCameraCommand;
import at.petritzdesigns.bitbot.networking.data.commands.ControlMotorCommand;
import at.petritzdesigns.bitbot.networking.data.commands.DistanceCommand;
import at.petritzdesigns.bitbot.networking.data.commands.EstablishConnectionCommand;
import at.petritzdesigns.bitbot.networking.data.commands.PanicCommand;
import java.util.HashMap;
import java.util.Map;

/**
 * The commands class. This is basically the storage of available commands.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Commands {

    /**
     * Instance for singleton pattern
     */
    private static Commands instance;

    /**
     * Registered commands
     */
    private final Map<Byte, Command> commands;

    /**
     * Default constructor<br>
     * creates empty map<br>
     * registers default commands
     */
    private Commands() {
        this.commands = new HashMap<>();
        registerDefaultCommands();
    }

    /**
     * Registers default commands
     */
    private void registerDefaultCommands() {
        registerCommand(new EstablishConnectionCommand());
        registerCommand(new CloseConnectionCommand());
        registerCommand(new PanicCommand());
        registerCommand(new ControlMotorCommand());
        registerCommand(new DistanceCommand());
        registerCommand(new ControlCameraCommand());
    }

    /**
     * Register specific command
     *
     * @param command the command
     */
    public void registerCommand(Command command) {
        commands.put(command.getIdentifier(), command);
    }

    /**
     * Returns registered commmands
     *
     * @return commands as map
     */
    public Map<Byte, Command> getRegisteredCommands() {
        return commands;
    }

    /**
     * Returns identifier by given class
     *
     * @param commandClass the class
     * @return id
     */
    public byte getIdentifierByClass(Class<?> commandClass) {
        for (Map.Entry<Byte, Command> command : commands.entrySet()) {
            if (command.getValue().getClass().equals(commandClass)) {
                return command.getKey();
            }
        }
        return (byte) -1;
    }

    /**
     * Singleton method
     *
     * @return instance
     */
    public static Commands getInstance() {
        if (instance == null) {
            instance = new Commands();
        }
        return instance;
    }
}
