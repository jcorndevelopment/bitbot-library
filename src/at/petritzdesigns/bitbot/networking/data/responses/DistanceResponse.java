package at.petritzdesigns.bitbot.networking.data.responses;

import java.util.List;

/**
 * The distance response. It returns the distance in mm. The inner class Data
 * then parses the distance to a long value.
 *
 * @see Data
 * @author Markus Petritz
 * @version 1.0.0
 */
public class DistanceResponse extends UniqueResponse {

    public DistanceResponse() {
        super((byte) 2, (short) 1);
    }

    @Override
    protected Response handle(short requestSequence, List<String> params) throws Exception {
        return new Data(identifier, requestSequence, params);
    }

    public class Data extends Response {

        private long distance;

        public Data(byte identifier, short requestSequence, List<String> parameter) {
            super(identifier, requestSequence, parameter);
            parseParameter();
        }

        private void parseParameter() {
            String param = parameter.get(0);
            distance = Long.parseLong(param);
        }

        public long getDistance() {
            return distance;
        }
    }
}
