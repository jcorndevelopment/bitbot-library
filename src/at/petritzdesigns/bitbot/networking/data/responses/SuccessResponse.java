package at.petritzdesigns.bitbot.networking.data.responses;

import java.util.List;

/**
 * The success response. This response is returned whenever a command execution
 * was successfull.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class SuccessResponse extends UniqueResponse {

    public SuccessResponse() {
        super((byte) 0, (short) 0);
    }

    @Override
    protected Response handle(short requestSequence, List<String> params) throws Exception {
        return new Data(identifier, requestSequence, params);
    }

    public class Data extends Response {

        public Data(byte identifier, short requestSequence, List<String> parameter) {
            super(identifier, requestSequence, parameter);
        }
    }

}
