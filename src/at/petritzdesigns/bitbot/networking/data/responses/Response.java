package at.petritzdesigns.bitbot.networking.data.responses;

import java.util.List;

/**
 * The response base class at client side. It should handle the parsing and is
 * mostly the inner class of UniqueResponses.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public abstract class Response {

    /**
     * Identifier of the response
     */
    protected final byte identifier;

    /**
     * Request sequence number
     */
    protected final short requestSequence;

    /**
     * The parameter list if any
     */
    protected final List<String> parameter;

    /**
     * Default Constructor
     *
     * @param identifier id
     * @param requestSequence request seq
     * @param parameter paramter
     */
    public Response(byte identifier, short requestSequence, List<String> parameter) {
        this.identifier = identifier;
        this.requestSequence = requestSequence;
        this.parameter = parameter;
    }

    public byte getIdentifier() {
        return identifier;
    }

    public List<String> getParameter() {
        return parameter;
    }

    public short getRequestSequence() {
        return requestSequence;
    }
}
