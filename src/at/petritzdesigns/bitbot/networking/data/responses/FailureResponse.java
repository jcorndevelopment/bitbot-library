package at.petritzdesigns.bitbot.networking.data.responses;

import java.util.List;

/**
 * The failure response. This response is returned whenever a command execution
 * failed.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class FailureResponse extends UniqueResponse {

    public FailureResponse() {
        super((byte) 1, (short) 0);
    }

    @Override
    protected Response handle(short requestSequence, List<String> params) throws Exception {
        return new Data(identifier, requestSequence, params);
    }

    public class Data extends Response {

        public Data(byte identifier, short requestSequence, List<String> parameter) {
            super(identifier, requestSequence, parameter);
        }
    }

}
