package at.petritzdesigns.bitbot.networking.data.responses;

import at.petritzdesigns.bitbot.exceptions.WrongParameterCountException;
import java.util.List;

/**
 * The real response base class. The parse method should return a object with a
 * class that is from the type Response.
 *
 * @see Response
 * @author Markus Petritz
 * @version 1.0.0
 */
public abstract class UniqueResponse {

    /**
     * Identifier of the response
     */
    protected final byte identifier;

    /**
     * Count of parameters, the response can return
     */
    private final short parameterCount;

    /**
     * Default Constructor
     *
     * @param identifier id
     * @param parameterCount paramter count
     */
    public UniqueResponse(byte identifier, short parameterCount) {
        this.identifier = identifier;
        this.parameterCount = parameterCount;
    }

    /**
     * Handles the parse function
     *
     * @param requestSequence request seq
     * @param params the parameters to parse
     * @return parsed response
     * @throws Exception when something failed
     */
    protected abstract Response handle(short requestSequence, List<String> params) throws Exception;

    /**
     * Checks if count is the expected parameter count
     *
     * @param count the real value
     * @return success
     */
    private boolean checkParameterCount(int count) {
        return parameterCount == count;
    }

    /**
     * Get the identifier of the response
     *
     * @return id
     */
    public byte getIdentifier() {
        return identifier;
    }

    /**
     * Parse the specified response
     *
     * @param requestSequence request seq
     * @param parameter the paramter
     * @return reponse
     * @throws WrongParameterCountException when a wrong parameter count is
     * supplied.
     */
    public Response parse(short requestSequence, List<String> parameter) throws Exception {
        if (checkParameterCount(parameter.size())) {
            return handle(requestSequence, parameter);
        } else {
            throw new WrongParameterCountException(parameter.size());
        }
    }
}
