package at.petritzdesigns.bitbot.networking.data;

import java.util.LinkedList;
import java.util.List;

/**
 * The response data. Contains only the data of responses and an implemented
 * toString method.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class ResponseData {

    /**
     * Identifier of the response
     */
    private final byte identifier;

    /**
     * Request sequence number
     */
    private final short requestSequence;

    /**
     * The parameter list if any
     */
    private final List<String> parameter;

    /**
     * Default Constructor
     *
     * @param identifier id
     * @param requestSequence request seq
     * @param parameters parameter
     */
    public ResponseData(byte identifier, short requestSequence, List<String> parameters) {
        this.identifier = identifier;
        this.requestSequence = requestSequence;

        if (parameters == null) {
            this.parameter = new LinkedList<>();
        } else {
            this.parameter = parameters;
        }
    }

    /**
     * Returns identifier of response
     *
     * @return id
     */
    public byte getIdentifier() {
        return identifier;
    }

    /**
     * Returns the request sequence number
     *
     * @return seq number
     */
    public short getRequestSequence() {
        return requestSequence;
    }

    /**
     * Get the parameter list
     *
     * @return parameter list
     */
    public List<String> getParameter() {
        return parameter;
    }

    /**
     * Return parameter as string, values seperated by a ";"
     *
     * @return param
     */
    public String parameterAsString() {
        StringBuilder sb = new StringBuilder();
        for (String param : parameter) {
            sb.append(param).append(";");
        }
        return sb.toString();
    }

    /**
     * Returns the response as string
     *
     * @return response as string
     */
    @Override
    public String toString() {
        return String.format("%d|%d|%s|bb", identifier, requestSequence, parameterAsString());
    }
}
