package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.responses.SuccessResponse;
import java.util.List;

/**
 * The establish connection command. This command sets up the connection and
 * calls the setup method on the robot. It also gives short pulses to the leds.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class EstablishConnectionCommand extends Command {

    /**
     * Default Constructor<br>
     * ID: 0<br>
     * Parameter: 0
     */
    public EstablishConnectionCommand() {
        super((byte) 0, (short) 0);
    }

    /**
     * Calls the robot setup method
     *
     * @param robot the robot
     * @param params the parameter
     * @return response
     * @throws Exception when something failed
     */
    @Override
    protected ResponseData handle(Roboter robot, short sequence, List<String> params) throws Exception {
        //out.println("establish");
        robot.getFrontLeds().turnOff();
        robot.getRearLeds().turnOff();

        System.out.println("pulse the leds");
        robot.getFrontLeds().pulse(1);
        robot.getRearLeds().pulse(1);
        robot.setup();

        return response(sequence, SuccessResponse.class);
    }

}
