package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.responses.SuccessResponse;
import java.util.List;

/**
 * The control motor command. It sets the speed of the motor to the specified
 * one in the parameter.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class ControlMotorCommand extends Command {

    /**
     * Default Constructor<br>
     * ID: 3<br>
     * Parameter: 2
     */
    public ControlMotorCommand() {
        super((byte) 3, (short) 2);
    }

    /**
     * Calls the Robot Methods to move left and right
     *
     * @param robot the robot
     * @param params the parameter
     * @return response
     * @throws Exception when something failed
     */
    @Override
    protected ResponseData handle(Roboter robot, short sequence, List<String> params) throws Exception {
        //out.format("control %s %s\n", params.get(0), params.get(1));

        int speedLeft = Integer.parseInt(params.get(0));
        int speedRight = Integer.parseInt(params.get(1));

        robot.getLeftMotor().move(speedLeft);
        robot.getRightMotor().move(speedRight);

        return response(sequence, SuccessResponse.class);
    }

}
