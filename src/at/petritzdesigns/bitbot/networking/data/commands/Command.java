package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.exceptions.WrongParameterCountException;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.Responses;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * The abstract command class. It handles the execution, parameter count
 * handling and has helper functions for the repsonses.
 *
 * @author Markus Petritz
 * @author Julian Maierl
 * @version 1.0.0
 */
public abstract class Command {

    /**
     * Identifier of the command
     */
    protected final byte identifier;

    /**
     * Excepted parameter count of the command
     */
    protected final short parameterCount;

    /**
     * Default Constructor
     *
     * @param identifier id
     * @param parameterCount param count
     */
    public Command(byte identifier, short parameterCount) {
        this.identifier = identifier;
        this.parameterCount = parameterCount;
    }

    /**
     * Method that will be executed when the command is called
     *
     * @param robot roboter instance
     * @param sequence the seq number
     * @param params the parameter
     * @return response data
     * @throws Exception when something failed
     */
    protected abstract ResponseData handle(Roboter robot, short sequence, List<String> params) throws Exception;

    /**
     * Checks if count is the expected parameter count
     *
     * @param count the real value
     * @return success
     */
    private boolean checkParameterCount(int count) {
        return parameterCount == count;
    }

    /**
     * Checks Parameter Count and then calls abstract method: handle
     *
     * @param robot roboter instance
     * @param sequence the seq number
     * @param params the parameter
     * @return response data
     * @throws Exception when something failed
     */
    public ResponseData execute(Roboter robot, short sequence, List<String> params) throws Exception {
        if (checkParameterCount(params.size())) {
            return handle(robot, sequence, params);
        } else {
            throw new WrongParameterCountException(params.size());
        }
    }

    /**
     * Creates response data
     *
     * @param sequence seq number
     * @param id id of the repsonse
     * @return reponse data
     */
    protected ResponseData response(short sequence, Class<?> id) {
        return response(sequence, id, new LinkedList<>());
    }

    /**
     * Creates response data
     *
     * @param sequence seq number
     * @param id id of the repsonse
     * @param params the paramter
     * @return reponse data
     */
    protected ResponseData response(short sequence, Class<?> id, List<String> params) {
        return new ResponseData(Responses.getInstance().getIdentifierByClass(id), sequence, params);
    }

    /**
     * Creates response data
     *
     * @param sequence seq number
     * @param id id of the repsonse
     * @param params the parameter
     * @return reponse data
     */
    protected ResponseData response(short sequence, Class<?> id, String... params) {
        return response(sequence, id, new ArrayList<>(Arrays.asList(params)));
    }

    /**
     * Returns identifier of command
     *
     * @return id
     */
    public byte getIdentifier() {
        return identifier;
    }
}
