package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.responses.SuccessResponse;
import java.util.List;

/**
 * The close connection command. It shutdowns the roboter and stops the
 * movement.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class CloseConnectionCommand extends Command {

    /**
     * Default Constructor<br>
     * ID: 1<br>
     * Parameter: 0
     */
    public CloseConnectionCommand() {
        super((byte) 1, (short) 0);
    }

    /**
     * Calls the shutdown method of the robot
     *
     * @param robot the robot
     * @param sequence the seq
     * @param params the parameter
     * @return response
     * @throws Exception when something failed
     */
    @Override
    protected ResponseData handle(Roboter robot, short sequence, List<String> params) throws Exception {
        //out.println("close");
        robot.getLeftMotor().move(0);
        robot.getRightMotor().move(0);
        robot.shutdown();

        return response(sequence, SuccessResponse.class);
    }

}
