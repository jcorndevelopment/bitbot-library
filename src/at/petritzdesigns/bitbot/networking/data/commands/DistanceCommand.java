package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.responses.DistanceResponse;
import java.util.List;

/**
 * The distance command. It aks the ultra sonic sensor for the distance 5 times
 * and then calculates the average distance. The distance is then returned by
 * the DistanceResponse.
 *
 * @see DistanceResponse
 * @version 1.0.0
 * @author Markus Petritz
 */
public class DistanceCommand extends Command {

    /**
     * Default Constructor<br>
     * ID: 4<br>
     * Parameter: 0
     */
    public DistanceCommand() {
        super((byte) 4, (short) 0);
    }

    @Override
    protected ResponseData handle(Roboter robot, short sequence, List<String> params) throws Exception {
        robot.getFrontLeds().turnOff();
        robot.getFrontLeds().pulse(500);
        long sum = 0;
        for (int i = 0; i < 5; i++) {
            sum += robot.getUltraSoundSensor().getDistance();
        }
        long avg = sum / 5;
        return response(sequence, DistanceResponse.class, avg + "");
    }

}
