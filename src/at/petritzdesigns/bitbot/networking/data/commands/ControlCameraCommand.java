package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.responses.SuccessResponse;
import java.util.List;

/**
 * The camera control command. It accepts a pan degree paramter and a tilt
 * degree parameter.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class ControlCameraCommand extends Command {

    public ControlCameraCommand() {
        super((byte) 5, (short) 2);
    }

    @Override
    protected ResponseData handle(Roboter robot, short sequence, List<String> params) throws Exception {
        //out.format("control camera %s %s\n", params.get(0), params.get(1));

        int panDegrees = Integer.parseInt(params.get(0));
        int tiltDegrees = Integer.parseInt(params.get(1));

        robot.getPanServo().setDegree(panDegrees);
        robot.getTiltServo().setDegree(tiltDegrees);

        return response(sequence, SuccessResponse.class);
    }

}
