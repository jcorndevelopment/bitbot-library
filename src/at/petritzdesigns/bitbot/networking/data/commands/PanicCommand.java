package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.responses.SuccessResponse;
import java.util.List;

/**
 * The panic command. It shutdowns the roboter immediatly.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class PanicCommand extends Command {

    /**
     * Default Constructor<br>
     * ID: 2<br>
     * Parameter: 0
     */
    public PanicCommand() {
        super((byte) 2, (short) 0);
    }

    /**
     * Calls the robot shutdown method, has high priority in queue
     *
     * @param robot the robot
     * @param params the parameter
     * @return response
     * @throws Exception when something failed
     */
    @Override
    protected ResponseData handle(Roboter robot, short sequence, List<String> params) throws Exception {
        //out.println("panic");
        robot.shutdown();

        return response(sequence, SuccessResponse.class);
    }

}
