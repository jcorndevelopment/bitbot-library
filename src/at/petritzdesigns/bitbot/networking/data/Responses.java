package at.petritzdesigns.bitbot.networking.data;

import at.petritzdesigns.bitbot.networking.data.responses.DistanceResponse;
import at.petritzdesigns.bitbot.networking.data.responses.FailureResponse;
import at.petritzdesigns.bitbot.networking.data.responses.SuccessResponse;
import at.petritzdesigns.bitbot.networking.data.responses.UniqueResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * The responses class. This is basically the storage of available responses.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Responses {

    /**
     * Instance for singleton pattern
     */
    private static Responses instance;

    /**
     * Registered responses
     */
    private final Map<Byte, UniqueResponse> responses;

    /**
     * Default constructor<br>
     * creates empty map<br>
     * registers default responses
     */
    private Responses() {
        this.responses = new HashMap<>();
        registerDefaultResponses();
    }

    /**
     * Registers default commands
     */
    private void registerDefaultResponses() {
        registerResponse(new SuccessResponse());
        registerResponse(new FailureResponse());
        registerResponse(new DistanceResponse());
    }

    /**
     * Register specific response
     *
     * @param response the response
     */
    public void registerResponse(UniqueResponse response) {
        responses.put(response.getIdentifier(), response);
    }

    /**
     * Returns registered responses
     *
     * @return commands as map
     */
    public Map<Byte, UniqueResponse> getRegisteredResponses() {
        return responses;
    }

    /**
     * Returns identifier by given class
     *
     * @param responseClass the class
     * @return id
     */
    public byte getIdentifierByClass(Class<?> responseClass) {
        for (Map.Entry<Byte, UniqueResponse> response : responses.entrySet()) {
            if (response.getValue().getClass().equals(responseClass)) {
                return response.getKey();
            }
        }
        return (byte) -1;
    }

    /**
     * Get the response object by identifier
     *
     * @param identifier the id
     * @return reference to object
     */
    public UniqueResponse getClassByIdentifier(byte identifier) {
        return responses.get(identifier);
    }

    /**
     * Singleton method
     *
     * @return instance
     */
    public static Responses getInstance() {
        if (instance == null) {
            instance = new Responses();
        }
        return instance;
    }
}
