package at.petritzdesigns.bitbot.networking.data;

import java.util.LinkedList;
import java.util.List;

/**
 * The response data factory. This class builds the ResponseData objects.
 * 
 * @see ResponseData
 * @author Markus Petritz
 * @version 1.0.0
 */
public class ResponseDataFactory {

    public ResponseDataFactory() {
    }
    
    /**
     * Gets response data with empty parameters
     *
     * @param id the id
     * @param requestSeq the request seq
     * @return response data
     */
    public ResponseData getResponseData(Class<?> id, short requestSeq) {
        return getResponseData(id, requestSeq, new LinkedList<>());
    }

    /**
     * Gets response data
     *
     * @param id the id
     * @param requestSeq the request seq
     * @param parameter the parameter
     * @return response data
     */
    public ResponseData getResponseData(Class<?> id, short requestSeq, List<String> parameter) {
        return new ResponseData(Responses.getInstance().getIdentifierByClass(id), requestSeq, parameter);
    }
}
