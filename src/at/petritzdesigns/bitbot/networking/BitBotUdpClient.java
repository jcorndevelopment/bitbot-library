package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.responses.Response;
import at.petritzdesigns.bitbot.networking.data.ServerPort;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * The BitBot client implementation for the UDP protocol.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotUdpClient extends BitBotClient {

    /**
     * Socket to send the data to the server
     */
    private DatagramSocket socket;

    /**
     * Default constructor, uses default port
     *
     * @param ip the ip
     */
    public BitBotUdpClient(String ip) {
        this(ip, ServerPort.DEFAULT_UDP_PORT.getPort());
    }

    /**
     * Constructor listens to local server with simulation port
     */
    public BitBotUdpClient() {
        this("127.0.0.1", ServerPort.SIMULATION_PORT.getPort());
    }

    /**
     * Uses custom port
     *
     * @param ip the ip adress
     * @param port custom port
     */
    public BitBotUdpClient(String ip, int port) {
        super(ip, port);
    }

    /**
     * Connects to the server
     *
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    @Override
    public void connectToServer() throws BitBotServerException {
        try {
            InetAddress address = InetAddress.getByName(ip);
            socket = new DatagramSocket();
            socket.connect(address, port);
            connected = true;
        } catch (SocketException ex) {
            throw new BitBotServerException(ex, "Cannot connect to server!");
        } catch (UnknownHostException ex) {
            throw new BitBotServerException(ex, "Cannot connect to server!");
        }
    }

    /**
     * Closes connection to the server
     *
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    @Override
    public void closeConnection() throws BitBotServerException {
        socket.disconnect();
        socket.close();
        connected = false;
    }

    /**
     * Sends the command to the server
     *
     * @param data The Command to send
     * @return response
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    @Override
    public Response sendCommand(CommandData data) throws BitBotServerException {
        try {
            InetAddress address = InetAddress.getByName(ip);
            if (!connected) {
                throw new BitBotServerException("Not connected!");
            }
            if (data == null) {
                throw new BitBotServerException("Command is null!");
            }
            byte[] sendData = data.toString().getBytes();
            socket.send(new DatagramPacket(sendData, sendData.length, address, port));
            //TODO: schauen wos zruck kimt und des senden
            return null;

        } catch (IOException ex) {
            throw new BitBotServerException(ex, "Could not send command to server!");
        }
    }

}
