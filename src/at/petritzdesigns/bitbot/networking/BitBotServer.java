package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The abstract BitBot server. It defines the basic methods for network
 * communication.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public abstract class BitBotServer {

    /**
     * ExecutorService for running the server
     */
    private ExecutorService es;

    /**
     * Roboter instance, where the commands are executed
     */
    protected final Roboter robot;

    /**
     * Port which the server should listen
     */
    protected final int port;

    /**
     * The queue where the commands are executed
     */
    protected CommandExecutor executor;

    /**
     * Whether the server is running or not
     */
    protected boolean running;

    /**
     * Instance of the Server class
     */
    private AbstractServer server;

    /**
     * Custom Constructor
     *
     * @param robot custom robot
     * @param port custom port
     */
    public BitBotServer(Roboter robot, int port) {
        this.robot = robot;
        this.port = port;
    }

    /**
     * Starts server
     *
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    public void start() throws BitBotServerException {
        executor = new CommandExecutor(robot);
        es = Executors.newSingleThreadExecutor();

        server = getServer();
        es.execute(server);
    }

    /**
     * Stops server
     */
    public void stop() {
        running = false;
        es.shutdown();
    }

    /**
     * Get the server to start
     *
     * @return server
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    protected abstract AbstractServer getServer() throws BitBotServerException;

    /**
     * Returns roboter instance
     *
     * @return roboter
     */
    public Roboter getRobot() {
        return robot;
    }

    /**
     * Returns port which server listens to
     *
     * @return port
     */
    public int getPort() {
        return port;
    }

    /**
     * Whether server is running or not
     *
     * @return running
     */
    public boolean isRunning() {
        return running;
    }
}
