package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.responses.Response;

/**
 * The abstract BitBot Client. It has the basic methods for the communication
 * defined.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public abstract class BitBotClient {

    /**
     * Port which client should listen to
     */
    protected final int port;

    /**
     * IP adress of the server
     */
    protected final String ip;

    /**
     * Whether client is connected to server or not
     */
    protected boolean connected;

    /**
     * Uses custom port
     *
     * @param ip the ip adress
     * @param port custom port
     */
    public BitBotClient(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    /**
     * Connects to the server
     *
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    public abstract void connectToServer() throws BitBotServerException;

    /**
     * Closes connection to the server
     *
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    public abstract void closeConnection() throws BitBotServerException;

    /**
     * Sends the command to the server
     *
     * @param data The Command to send
     * @return response
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    public abstract Response sendCommand(CommandData data) throws BitBotServerException;

    /**
     * Returns port
     *
     * @return port
     */
    public int getPort() {
        return port;
    }

    /**
     * Returns ip adress
     *
     * @return ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Whether client is connected to a server or not
     *
     * @return connected
     */
    public boolean isConnected() {
        return connected;
    }
}
