package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.Commands;
import at.petritzdesigns.bitbot.networking.data.ResponseData;
import at.petritzdesigns.bitbot.networking.data.commands.Command;
import at.petritzdesigns.bitbot.networking.data.commands.EstablishConnectionCommand;
import java.io.PrintStream;
import java.util.LinkedList;

/**
 * The command executor. This class executes the commands on the robot.
 *
 * @author Markus Petritz
 * @author Julian Maierl
 * @version 1.0.0
 */
public class CommandExecutor {

    /**
     * Roboter instance
     */
    private final Roboter robot;

    /**
     * Output instance
     */
    private PrintStream output;

    /**
     * Constructor<br>
     * Registers default commands<br>
     * Creates empty Queue<br>
     * Builts Executor service with cached thread pool
     *
     * @param robot robot instance
     */
    public CommandExecutor(Roboter robot) {
        this(robot, System.out);
    }

    /**
     * Constructor<br>
     * Registers default commands<br>
     * Creates empty queue<br>
     * Builts Executor service with single thread pool
     *
     * @param robot robot instance
     * @param output output instance
     */
    public CommandExecutor(Roboter robot, PrintStream output) {
        this.robot = robot;
        this.output = output;
    }

    public void setup() {
        System.out.println("setup robot");
        robot.getSwitch().addListener((boolean isPressed) -> {
            try {
                System.out.println("pressed=" + isPressed);
                if (isPressed) {
                    robot.getFrontLeds().turnOff();
                    robot.getRearLeds().turnOff();
                }
            } catch (Exception e) {
                System.err.println(e);
            }
        });
    }

    public void setOutput(PrintStream output) {
        this.output = output;
    }

    /**
     * Adds command data to the queue<br>
     * Shutdowns everything if a panic command is sent
     *
     * @param data command data
     * @return response data
     * @throws java.lang.Exception on errors
     */
    public ResponseData run(CommandData data) throws Exception {
        if (Commands.getInstance().getRegisteredCommands().containsKey(data.getIdentifier())) {
            Command command = Commands.getInstance().getRegisteredCommands().get(data.getIdentifier());
            ResponseData response = command.execute(robot, data.getSequence(), data.getParameter());

            if (command instanceof EstablishConnectionCommand) {
                setup();
            }

            return response;
        }

        return new ResponseData((byte) 1, data.getSequence(), new LinkedList<>());
    }
}
