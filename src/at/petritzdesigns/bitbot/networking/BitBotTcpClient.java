package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.responses.Response;
import at.petritzdesigns.bitbot.networking.data.ServerPort;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * The BitBot client implementation for the TCP protocol.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotTcpClient extends BitBotClient {

    /**
     * PrintWriter to send data to the server!
     */
    private PrintWriter pw;

    /**
     * Reads se output from se server.
     */
    private BufferedReader br;

    /**
     * Socket to send the data to the server
     */
    private Socket socket;

    /**
     * Default constructor, uses default port
     *
     * @param ip the ip
     */
    public BitBotTcpClient(String ip) {
        this(ip, ServerPort.DEFAULT_TCP_PORT.getPort());
    }

    /**
     * Constructor listens to local server with simulation port
     */
    public BitBotTcpClient() {
        this("127.0.0.1", ServerPort.SIMULATION_PORT.getPort());
    }

    /**
     * Uses custom port
     *
     * @param ip the ip adress
     * @param port custom port
     */
    public BitBotTcpClient(String ip, int port) {
        super(ip, port);
    }

    /**
     * Connects to the server
     *
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    @Override
    public void connectToServer() throws BitBotServerException {
        try {
            socket = new Socket(ip, port);
            pw = new PrintWriter(socket.getOutputStream());
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            connected = true;
        } catch (IOException ex) {
            throw new BitBotServerException(ex, "Cannot connect to server!");
        }
    }

    /**
     * Closes connection to the server
     *
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    @Override
    public void closeConnection() throws BitBotServerException {
        try {
            socket.close();
            connected = false;
        } catch (IOException ex) {
            throw new BitBotServerException(ex, "Cannot close the connection!");
        }
    }

    /**
     * Sends the command to the server
     *
     * @param data The Command to send
     * @return response
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException on
     * errors
     */
    @Override
    public Response sendCommand(CommandData data) throws BitBotServerException {
        try {
            if (!connected) {
                throw new BitBotServerException("Not connected!");
            }
            if (data == null) {
                throw new BitBotServerException("Command is null!");
            }

            pw.println(data.toString());
            pw.flush();

            String line = br.readLine();
            System.out.println("Response: [" + line + "]");

            return BitBotCommandProtocol.parseResponse(line);
        } catch (Exception ex) {
            throw new BitBotServerException(ex, "Could not send command to server!");
        }
    }

}
