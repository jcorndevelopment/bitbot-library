package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.networking.data.CommandData;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BBCPTest {
    
    public BBCPTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of parseCommand method, of class BBCP.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testParseCommand() throws Exception {
        System.out.println("parseCommand");
        
        String line = "0|0|abc;100;def|bb";
        CommandData expResult = new CommandData((byte) 0, (short) 0, new ArrayList<>(Arrays.asList("abc", "100", "def")));
        CommandData result = BitBotCommandProtocol.parseCommand(line);
        assertEquals(expResult, result);
    }
    
}
